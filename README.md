# How to override methods in unit test using build tags

## TL;DR

We are using build tags to control which function should be used.

## Explanation

We define a person struct with a SayHello() and a greet() method. SayHello calls the private function greet. This is the function we want to override while in a unit test. This could also be a call to an external API, which is something we don't want in a test. A unit test should be isolated from the outside and not fail due to things we can't control, like the API being unavailable because of network connectivity issues for example. We fix this by only doing our API call when actually compiling for production (or e2e testing). When running unit tests we can pass a list of tags to go. We use this tag list to decide, which function should end up in the binary. When we pass a `unit` flag we include the overriden method, denoted by the first two lines in `main_test.go`:

```go
//go:build unit
// +build unit

package main

import "testing"

```

In all other cases we want to build with the function from `greet.go`, again denoted by the following build flags:

```go
//go:build !unit
// +build !unit

package main

// compile with this method, if were not running a unit test
func (p *Person) greet() {
	println("Hello from Main")
}
```

Notice that we're using the `!` to state that we always want to include this except when compiling with the tag `unit`.

We can run the tests with `go test --tags unit -v` (-v to show the print statements). The output looks something like this:

```
=== RUN   Test_main
Hello from Tests
--- PASS: Test_main (0.00s)
PASS
ok      example 0.003s
```

We can also run `go run ./...` to see that when building for production, we are in fact using the original method from `greet.go`:

```
$ go run ./...
Hello from Main
```
