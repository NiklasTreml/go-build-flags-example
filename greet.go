//go:build !unit
// +build !unit

package main

// compile with this method, if were not running a unit test
func (p *Person) greet() {
	println("Hello from Main")
}
