//go:build unit
// +build unit

package main

import "testing"

func (p *Person) greet() {
	println("Hello from Tests")
}

func Test_main(t *testing.T) {
	person := Person{}
	person.SayHello()

}
